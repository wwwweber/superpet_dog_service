package com.superpet.weixin.api.User;

import com.jfinal.kit.Ret;
import com.superpet.common.controller.BaseController;
import com.superpet.common.kits.ConstantKit;
import com.superpet.common.model.Users;

public class UserController extends BaseController {

    static UserService srv = UserService.me;
    
    public void updateUser(){
        Users user = srv.getUserByOpenid(getWxSession().getOpenid());
        if(user!=null){
            user.setNickName(getPara("nickName"));
            user.setAvatar(getPara("avatarUrl"));
            user.setSex(getParaToInt("gender"));
            user.setCity(getPara("city"));
            user.setProvince(getPara("province"));
            user.setCountry(getPara("country"));
            user.setLanguage(getPara("language"));
            user.update();
            renderJson(Ret.ok("code",ConstantKit.CODE_SUCCESS).set("msg",ConstantKit.MSG_SUCCESS).set("userId",user.getId()));
        }
    }

}
