package com.superpet.common.kits;

import java.util.Random;

public class NumberKit {

	/**
	 * 取出一个指定长度大小的随机正整数.
	 * 
	 * @param length
	 *            int 设定所取出随机数的长度。length小于11
	 * @return int 返回生成的随机数。
	 */
	public static int buildRandom(int length) {
		int num = 1;
		double random = Math.random();
		if (random < 0.1) {
			random = random + 0.1;
		}
		for (int i = 0; i < length; i++) {
			num = num * 10;
		}
		return (int) ((random * num));
	}

	//获取一个数的1/4至该数3/4之间的一个随机数
	public static Integer getRandom(int num){
		int min = num / 4;
		int max	= 3 * (num / 4);
		Random random = new Random();
		int s = random.nextInt(max) % (max - min + 1) + min;
		return s;
	}

	//获取一个数的0至该数之间的一个随机数
	public static Integer getFullRandom(int num){
		int min = 0,max	= num;
		Random random = new Random();
		int s = random.nextInt(max) % (max - min + 1) + min;
		return s;
	}

}
